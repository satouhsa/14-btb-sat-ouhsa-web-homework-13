import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import{BrowserRouter as Router,Switch,Route} from 'react-router-dom';
import Menu from './Component/Menu';
import Home from './Component/Home';
import Add from './Component/Add';
import View from './Component/View';
import Delete from './Component/Delete';
import Update from './Component/Update';
function App() {
  return (
    <div className="App">
     
     <Router>
        <Menu/>
          <Switch>
        
           
            <Route path='/Home' exact component={Home}/>
            <Route path='/Add' component={Add}/>
            <Route path='/View/id=:id' component={View}/>
            <Route path='/Delete/:id' component={Delete}/>
            <Route path='/Update/id=:id' component={Update}/>
            
           
          </Switch>
      </Router>

    </div>
  );
}

export default App;

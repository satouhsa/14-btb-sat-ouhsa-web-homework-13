import React, { Component } from 'react';
import axios from 'axios';

export default class View extends Component {



    constructor(){
        super();
        this.state={
            title:"",
            description:"",
            image:"",
        }
    }

    componentWillMount(){
    
        console.log("This is ID: "+this.props.match.params.id);
        axios.get( `http://110.74.194.124:15011/v1/api/articles/${this.props.match.params.id}`).then((res)=>{

       
            
               this.setState({
                    title:res.data.DATA.TITLE,
                    description:res.data.DATA.DESCRIPTION,
                    image:res.data.DATA.IMAGE

                    
               }); 
            
        });
      
    }
    render() {
        return (
            <div>
                
                <br/>

                <div className="container">
                    <h3>Article</h3>

                            <br/>
                    
                    <div className="row">


                        <div className="col-md-3">


                            <img width="200" src={this.state.image}/>
                        </div>
                        <div className="col-md-4">
                                  {this.state.title}
                                  <br/>

                                  {this.state.description}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {Form,Button,Label} from 'react-bootstrap';
import axios from 'axios';


export default class Add extends Component {


            constructor(props){

                super(props);

                this.state={
                    article:[],
                    title:'',
                    description:'',
                   titleerror:'',
                   descriptionerror:'',
                   imagePreviewUrl: ''

                   
                    
                }
            

            }
            componentWillMount(){
                console.log("Will Mount");
                axios.get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15").then((res)=>{
        
               
                    
                       this.setState({
                            article:res.data.DATA
                            
                       });
                   
        
                        
                });
              
            }
           

            onImageChange = (event) => {
                console.log(event);
                if (event.target.files && event.target.files[0]) {
                  this.setState({
                    imagePreviewUrl: URL.createObjectURL(event.target.files[0]),
                  });
                }
              };
          

            handleChange = (event) => {
               
                this.setState({
                    [event.target.name]:event.target.value,
                    titleerror:false,
                    descriptionerror:false
                })
        
               
                }
            
           

              handleSubmit = (event) => {
                event.preventDefault();

                if(this.state.title==""&& this.state.description==""){
                    this.setState({
                        titleerror:true,
                        descriptionerror:true
                    })
                    return;
                }else if(this.state.title==""){
                    this.setState({
                        titleerror:true
                    })
                }else if(this.state.description==""){
                    this.setState({
                        descriptionerror:true
                    })
                }
                
                let addnew={
                    TITLE: this.state.title,
                    DESCRIPTION: this.state.description,
                    IMAGE: this.state.imagePreviewUrl
                    
                }
                console.log(this.state.imagePreviewUrl);
                axios.post("http://110.74.194.124:15011/v1/api/articles",addnew).then((res)=>{
                       
                      alert(res.data.MESSAGE)
                        
                      window.history.back();
                     
             });

               
              }

            
    render() {

        
        return (
            <div>
                
                <br/>

                <p style={{textAlign:"left",marginLeft:250}}>
                    <h3><b>Add Article</b> </h3>

                    <br/>

                    <Form onSubmit={this.handleSubmit} noValidate>

                        <div className="row">
                            <div className="col-md-5">

                                        <div className="title">
                                        <Form.Label>Title</Form.Label>
                                        <br/>
                                           
                                           <input type='text' name='title' value={this.state.title} style={{width:"450px",padding:"5px"}} onChange={(e)=>this.handleChange(e)}  />
                                           <br/>
                                             <label style={{color:"red"}}>{this.state.titleerror ? "vaild input":""}</label>

                                        </div>
                                          
                                         
                                           

                                        <div className="description">
                                        <label>Description</label>
                                        <br/>
                                            
                                            <input type='text' name='description' value={this.state.description} style={{width:"450px",padding:"5px"}} onChange={(e)=>this.handleChange(e)}  />
                                            <br/>
                                             <label style={{color:"red"}}> { this.state.descriptionerror ? "vaild input":""}</label>

                                        </div>
                                           

                                            
                                          
                                                

                            </div>
                            <div className="col-md-5">
                                    
                                    <input className="fileInput" type="file" onChange={(e)=>this.onImageChange(e)}/>
                                    <img src={this.state.imagePreviewUrl} width="300" height="200" />
                                   
                                
                                
                            </div>
                        </div>
                        <Button onClick={(e)=>this.handleSubmit(e)}>Submit</Button>
                            
                    </Form>
                </p>




            </div>
        )
    }
}

import React, { Component } from 'react';
import {Form,Button} from 'react-bootstrap';
import axios from 'axios';

export default class Update extends Component {

        constructor(){
            super();
            this.state={
               title:'',
               description:'',
               imagePreviewUrl: ''
                

            }

        }
        onImageChange = (event) => {
            console.log(event);
            if (event.target.files && event.target.files[0]) {
              this.setState({
                imagePreviewUrl: URL.createObjectURL(event.target.files[0]),
              });
            }
          };

        onUpdate(e){
            e.preventDefault();
            const Update ={
                TITLE:this.state.title,
                DESCRIPTION:this.state.description,
                IMAGE:this.state.imagePreviewUrl,    
            }
            const id = this.props.match.params.id;
            console.log(this.state.imagePreviewUrl);
            axios.put(`http://110.74.194.124:15011/v1/api/articles/${id}`,Update).then(res =>{
                console.log(res.data);
                alert(res.data.MESSAGE);
                window.history.back();
                
            })
            console.log(this.state.title)
    
        }

        componentWillMount(){
            const ID = this.props.match.params.id;
            console.log(this.props.match.params.id);
            axios.get( `http://110.74.194.124:15011/v1/api/articles/${ID}`).then((res)=>{

               
                this.setState({
                    data: res.data.DATA,
                    title: res.data.DATA.TITLE,
                    description: res.data.DATA.DESCRIPTION,
                    image: res.data.DATA.IMAGE
                    
    
                }) ;   
                console.log(this.state.title)     
            })
        }

        changeHandler= (e) => {
            this.setState({ [e.target.name]: e.target.value}
 
                )
        }   
        
       

    render() {
        return (
            <div>
                

                <br/>

                        <p style={{textAlign:"left",marginLeft:250}}>
                            <h3><b>Update Article</b> </h3>

                            <br/>

                            <Form onSubmit ={(e)=>this.onUpdate(e)}>

                                <div className="row">
                                    <div className="col-md-5">

                                                <Form.Group controlId="formGroupEmail">
                                                    <Form.Label>Title</Form.Label>
                                                    <Form.Control type="text" name="title" value={this.state.title} placeholder="Enter title" onChange={(e)=>this.changeHandler(e)} />
                                                </Form.Group>
                                                <Form.Group controlId="formGroupPassword">
                                                    <Form.Label>Description</Form.Label>
                                                    <Form.Control type="text" name="description" value={this.state.description} placeholder="Enter description" onChange={(e)=>this.changeHandler(e)} />
                                                </Form.Group>
                                    </div>
                                    <div className="col-md-5">
                                            {/* <img src={require('./image/poto.jpg')} width="300" height="200" /> */}

                                            <input className="fileInput" type="file" onChange={(e)=>this.onImageChange(e)}/>
                                             <img src={this.state.imagePreviewUrl} width="300" height="200" />
                                   
                                
                                        
                                    </div>
                                </div>
                                <Button type="submit" >Save</Button>
                                    
                            </Form>
                        </p>

            </div>
        )
    }
}

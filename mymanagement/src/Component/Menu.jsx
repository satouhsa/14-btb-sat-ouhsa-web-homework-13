import React from 'react';
import{Navbar,Nav,Button,Form,FormControl} from 'react-bootstrap';
import {Link} from 'react-router-dom';

function Menu() {
    return (
        <div>

            <div >

                    <Navbar bg="dark" variant="dark">
                        <Navbar.Brand as= {Link} to="">AMS</Navbar.Brand>
                        <Nav className="mr-auto">
                        <Nav.Link as= {Link} to="/Home" >Home</Nav.Link>
                        
                        </Nav>
                        <Form inline>
                        <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                        <Button variant="outline-info">Search</Button>
                        </Form>
                </Navbar>
            </div>
            
            

        </div>
    )
}

export default Menu

import React, { Component } from 'react';
import{Button,Table,thead,tbody} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import axios from 'axios';
            

export default class Home extends Component {

    constructor(){
        super();
        this.state={
            users:[
               
            ],
        };
        this.handleDelete=this.handleDelete.bind(this);
    }
    handleDelete(id){
        axios.delete(`http://110.74.194.124:15011/v1/api/articles/${id}`)
        .then(res=>{console.log(res.data) 
            this.componentWillMount() });

        alert("delete successfully");
    }

     convertToDate(date) {
        let paramDate=date;
        let year=paramDate.substring(0,4);
        let month=paramDate.substring(4,6);
        let day=paramDate.substring(6,8);
        let fullDate=year+"-"+month+"-"+day;
        return fullDate;
        
    }
    componentWillMount(){
        console.log("Will Mount");
        axios.get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15").then((res)=>{

       
            
               this.setState({
                    users:res.data.DATA
                    
               });
           

                
        });
      
    }
    render() {
     
        return (
            <div>
                

               
                <div className="title">


                    <br/>

                    
                            <p style={{textAlign:"center"}}>
                            <h2>Article Management</h2>

                            <br/>

                            <Button><Link to={"/Add"}><span style={{color:'black'}}>Add new Article</span></Link></Button>

                    </p>
                   
                </div>
                <br/>

                <div className="table">


                        <div className="container" >

                                    <Table striped bordered hover>
                                        <thead style={{background:"black"}}>
                                            <tr>
                                            <th style={{textAlign:"center",color:"white"}} >#</th>
                                            <th style={{textAlign:"center",color:"white"}} >Title</th>
                                            <th style={{textAlign:"center",color:"white"}} >Description</th>
                                            <th style={{textAlign:"center",color:"white"}} >Create Date</th>
                                            <th style={{textAlign:"center",color:"white"}} >Image</th>
                                            <th style={{textAlign:"center",color:"white"}} >Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        
                                            {this.state.users.map((user)=><tr key={user.ID}>

                                                    <td style={{width:'5%'}}>{user.ID}</td>
                                                    <td style={{width:'20%'}}>{user.TITLE}</td>
                                                    <td style={{width:'20%'}}>{user.DESCRIPTION}</td>
                                                    <td style={{width:'10%'}}>{this.convertToDate(user.CREATED_DATE)}</td>
                                                    <td style={{width:'10%'}}><img src={user.IMAGE} style={{width:'100px'}}/></td>
                                                    <td style={{width:'20%'}}>
                                                    <Button variant="primary"> <Link to={  `/Update/id=${user.ID}`}  ><span style={{color:'white'}}>update</span></Link> </Button>&nbsp;
                                                    <Button variant="warning">  <Link to={`/View/id=${user.ID}`}><span style={{color:'white'}}>view</span></Link></Button> &nbsp;
                                                    <Button variant="danger" onClick={(id)=>this.handleDelete(user.ID)}><span style={{color:'white'}}> delete</span></Button>
                                                       
                                                        
                                                          
                                                    </td>

                                            </tr>
                                                
                                            )}
                                        
                                        </tbody>
                                        </Table>


                        </div>
              
                </div>
            </div>
        )
    }


}
